#!/usr/bin/env python
# coding: utf-8

# # Space Race Analysis (USA vs USSR)
# #By- Aarush Kumar
# #Dated: July 03,2021

# In[2]:


get_ipython().system('pip install geopandas')


# In[3]:


import numpy as np
import pandas  as pd
import matplotlib.pyplot as plt
import geopandas as gpd
import seaborn as sns
sns.set()


# In[4]:


Sp_missions_df=pd.read_csv('/home/aarush100616/Downloads/Projects/Space Race Analysis/Space_Corrected.csv')


# In[5]:


Sp_missions_df.head()


# In[6]:


Sp_missions_df.info()


# In[7]:


Sp_missions_df.isnull().sum()


# In[8]:


Sp_missions_df.shape


# In[9]:


Sp_missions_df.size


# In[10]:


Sp_missions_df.describe()


# ## Removing Unwanted columns

# In[11]:


Sp_missions_df=Sp_missions_df.drop(['Unnamed: 0','Unnamed: 0.1'],axis=1)


# In[12]:


Sp_missions_df['Year']=pd.to_datetime(Sp_missions_df['Datum']).apply(lambda x : x.year)
Sp_missions_df['Month']=pd.to_datetime(Sp_missions_df['Datum']).apply(lambda x : x.month)
Sp_missions_df['Day']=pd.to_datetime(Sp_missions_df['Datum']).apply(lambda x : x.day)


# In[13]:


Sp_missions_df['Country']=Sp_missions_df['Location'].apply(lambda x : x.strip().split(",")[-1])


# ## EDA using seaborn and matplotlib

# In[14]:


fig, ax= plt.subplots(figsize =(12,8))
splot=sns.countplot(data=Sp_missions_df,x='Status Mission')
for p in splot.patches:
    splot.annotate(format(p.get_height(), '.0f'), (p.get_x() + p.get_width() / 2., p.get_height()), ha = 'center', va = 'center', xytext = (0, 10), textcoords = 'offset points')
plt.title('Status of mission from 1957 onwards')
plt.xlabel("Mission status")
plt.show()


# In[15]:


Details=Sp_missions_df.groupby('Status Mission')['Detail'].count()
List_Mission_status=['Failure','Partial Failure','Prelaunch Failure','Success']


# In[16]:


fig, ax= plt.subplots(figsize =(12,20))
plt.pie(Details,labels=List_Mission_status,autopct='%1.2f%%')    
plt.show()


# In[17]:


fig, ax= plt.subplots(figsize =(12,8))
cplot=sns.countplot(data=Sp_missions_df,x='Status Rocket')
for p in cplot.patches:
    cplot.annotate(format(p.get_height(),'.0f'),(p.get_x() + p.get_width() / 2., p.get_height()), ha = 'center', va = 'center', xytext = (0, 10), textcoords = 'offset points')
plt.title('Roceket status used for mission from  1957 onwards')
plt.show()


# In[18]:


RocketDetails=Sp_missions_df.groupby('Status Rocket')['Detail'].count()
List_Mission_status=['StatusActive','StatusRetired']
fig, ax= plt.subplots(figsize =(12,20))
plt.pie(RocketDetails,labels=List_Mission_status,autopct='%1.2f%%')    
plt.show()


# In[19]:


fig, ax= plt.subplots(figsize =(20,20))
cplot=sns.countplot(data=Sp_missions_df,x='Status Rocket',hue='Country')
for p in cplot.patches:
    cplot.annotate(format(p.get_height(),'.0f'),(p.get_x() + p.get_width() / 2., p.get_height()), ha = 'center', va = 'center', xytext = (0, 10), textcoords = 'offset points')
plt.title('Roceket status used for mission from  1957 onwards country wise')
plt.xlabel('Rocket status')
plt.show()


# In[20]:


fig, ax= plt.subplots(figsize =(20,20))
cplot=sns.countplot(data=Sp_missions_df,y='Company Name')
for p in cplot.patches:
    cplot.annotate(format(p.get_width(),'.0f'),(p.get_x() + p.get_width()+25, p.get_y()+0.90), ha = 'center', va = 'center', xytext = (0, 10), textcoords = 'offset points')
plt.title('Number of missions by each organizaation ')
plt.xlabel("Number of missions")
plt.ylabel("Organization names")
plt.show()


# In[21]:


fig, ax= plt.subplots(figsize =(20,20))
cplot=sns.countplot(data=Sp_missions_df,y='Year',color='#93d498')
for p in cplot.patches:
    cplot.annotate(format(p.get_width(),'.0f'),(p.get_x() + p.get_width()+3, p.get_y()+0.90), ha = 'center', va = 'center', xytext = (0, 10), textcoords = 'offset points')
plt.title('Space missions eah year')
plt.ylabel("Year")
plt.xlabel('Number of space missions')
plt.show()


# In[22]:


Sp_missions_df_1971=Sp_missions_df[Sp_missions_df['Year']==1971]


# In[23]:


Sp_missions_df_1971


# In[24]:


fig, ax= plt.subplots(figsize =(20,20))
cplot=sns.countplot(data=Sp_missions_df_1971,y='Country')
for p in cplot.patches:
    cplot.annotate(format(p.get_width(),'.0f'),(p.get_x() + p.get_width()+2, p.get_y()+0.5), ha = 'center', va = 'center', xytext = (0, 10), textcoords = 'offset points')
plt.title('Nation wise space missions during 1971')
plt.ylabel('Country Name')
plt.xlabel('Number of missions')
plt.show()


# In[25]:


countries_list = list()
frequency_list = list()
test = Sp_missions_df.groupby("Country")["Company Name"].unique()
for i in test.iteritems():
    countries_list.append(i[0])
    frequency_list.append(len(i[1]))
    
companies = pd.DataFrame(list(zip(countries_list, frequency_list)), columns =['Country', 'Company Number'])
companies = companies.sort_values("Company Number", ascending=False)
companies


# In[26]:


fig,ax=plt.subplots(figsize=(12,12))
barplot=sns.barplot(data=companies,y='Country',x='Company Number')
plt.ylabel("Country Name")
plt.xlabel('Number of Companies or Space organizations')
plt.show()


# In[27]:


countries_list = list()
frequency_list = list()
test = Sp_missions_df.groupby("Country")["Detail"].unique()
for i in test.iteritems():
    countries_list.append(i[0])
    frequency_list.append(len(i[1]))
    
companies_misiion_count = pd.DataFrame(list(zip(countries_list, frequency_list)), columns =['Country', 'Space Mission count'])
companies_misiion_count = companies_misiion_count.sort_values("Space Mission count", ascending=False)
companies_misiion_count


# In[28]:


fig,ax=plt.subplots(figsize=(12,12))
barplot=sns.barplot(data=companies_misiion_count,y='Country',x='Space Mission count')
plt.title('Space missions country wise')
plt.ylabel('Country Name')
plt.xlabel('Number of space missions')
plt.show()

